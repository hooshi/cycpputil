#include <cstdio>

#include "../cycpputil.hxx"

namespace cycpputil_test
{
#define HAVE_TEST(name) extern void name(FILE *)
HAVE_TEST(dict);
HAVE_TEST(vector);
HAVE_TEST(basic_types);
HAVE_TEST(call_python_basic);
HAVE_TEST(call_python_advanced);
#undef HAVE_TEST
}

int
main(int argc, char * argv[])
{
  // Must create this
  cycpputil::Py_init py_init;

  // Empty executable to generate valgrdin suppressions
  if((argc == 2) && (strcmp(argv[1], "empty") == 0))
  {
    return 0;
  }

  const char * c_lines = "===========================";
  const char * c_green = "\033[1;32m";
  const char * c_red = "\033[1;31m";
  const char * c_reset = "\033[0m";
  CYCPPUTIL_UNUSED(c_red);
  FILE * fout = stderr;

  auto test_begin = [&](const char * n) {
    fprintf(fout, "%s %s TESTING %s %s %s\n", c_green, c_lines, n, c_lines, c_reset);
  };
  auto test_end = [&]() { //
    // fprintf(fout, "%s %s DONE %s %s\n", c_green, c_lines, c_lines, c_reset);
  };

  try
  {
    test_begin("VECTOR");
    cycpputil_test::vector(fout);
    for(unsigned i = 0; i < 10; ++i) // test more for memory leak
      cycpputil_test::vector(NULL);
    test_end();

    test_begin("BASIC TYPES");
    cycpputil_test::basic_types(fout);
    test_end();

    test_begin("DICT");
    cycpputil_test::dict(fout);
    for(unsigned i = 0; i < 100; ++i) // test more for memory leak
      cycpputil_test::dict(NULL);
    test_end();

    test_begin("CALL MODULE");
    // Add the path of the modules to python path (comes from cmake).
    cycpputil::pypath_append( CYCPPUTIL_TEST_MODULES_PATH );
    cycpputil_test::call_python_basic(fout);
    // Packages are messed up for the self built python
    // So don't test them
#if !defined(CYCPPUTIL_USE_SELF_BUILT_PYTHON)
    cycpputil_test::call_python_advanced(fout);
#endif
    test_end();
  }
  catch(cycpputil::Error err)
  {
    CYCPPUTIL_TERMINATE_ERR(err, fout);
  }

  return 0;
}
cmake_minimum_required(VERSION 3.5)
project(cycpputil)

set(SOURCES_LIB
  cycpputil.hxx
  cycpputil.cxx
  )

set(SOURCES_TEST
  test/main.cxx
  test/vector.cxx
  test/dict.cxx
  test/basic_types.cxx
  test/call_python.cxx
  )

# hardcode
string( TOLOWER  "${CMAKE_BUILD_TYPE}" _BUILD_TYPE)
if ("${_BUILD_TYPE}" STREQUAL "debug" AND CYCPPUTIL_USE_SELF_BUILT_PYTHON)
  add_definitions( "-DCYCPPUTIL_USE_SELF_BUILT_PYTHON=1")
  set( PYTHON_INCLUDE_DIR "/home/hooshi/code/cpython-3.6.7/debug/install/include/python3.6dm/" )
  set( PYTHON_LIBRARY  "/home/hooshi/code/cpython-3.6.7/debug/install/lib/libpython3.6dm.a" )
  find_package (Threads)
  set( PYTHON_EXTRA_LIBRARIES  ${CMAKE_THREAD_LIBS_INIT}  ${CMAKE_DL_LIBS} util )
endif()

find_package(PythonLibs 3.1 REQUIRED)
if( NOT PYTHONLIBS_FOUND )
  message(FATAL_ERROR "PYTHONLIBS was not found, cannot build tests ")
endif()

# Set the path to the test modules
add_definitions( "-DCYCPPUTIL_TEST_MODULES_PATH=\"${CMAKE_CURRENT_SOURCE_DIR}/test\"")
add_definitions( "-Wall -pedantic")

include_directories( ${PYTHON_INCLUDE_DIRS} )

add_executable( cycpputil_test ${SOURCES_LIB} ${SOURCES_TEST})
target_link_libraries( cycpputil_test 
${PYTHON_LIBRARIES}
${PYTHON_EXTRA_LIBRARIES}
 )
framework_hooshi_setup_exe_location(  cycpputil_test )

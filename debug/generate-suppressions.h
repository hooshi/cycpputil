#! /usr/bin/bash

set -x

valgrind --leak-check=full --show-reachable=yes --error-limit=no --gen-suppressions=all --log-file=minimalraw.log ./bin/cycpputil_test empty

cat ./minimalraw.log | ../valgrind/parse-suppressions.sh > valgrind-python.supp

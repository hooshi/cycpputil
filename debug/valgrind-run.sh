#! /usr/bin/env bash

set -x

valgrind --leak-check=full --show-reachable=yes --error-limit=no  --suppressions=valgrind-python.supp ./bin/cycpputil_test 
